#!/usr/bin/perl

# tweak the HTML output to work better with Bootstrap themes

package IkiWiki::Plugin::bootstrap;

use warnings;
use strict;
use IkiWiki 3.00;

sub import {
	hook(type => "getsetup", id => "bootstrap", call => \&getsetup);
	hook(type => "sanitize", id => "bootstrap", call => \&sanitize);
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => undef,
			section => "chrome",
		},
}

sub sanitize (@) {
	my %params=@_;
	$params{content} =~ s/<table>/<table class="table">/g;
	return $params{content};
}

1;
