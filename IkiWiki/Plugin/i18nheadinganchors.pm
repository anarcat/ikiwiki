#!/usr/bin/perl
# quick HTML heading id adder by Paul Wise
# modified for better i18n support by Antoine Beaupré
package IkiWiki::Plugin::i18nheadinganchors;

use warnings;
use strict;
use IkiWiki 3.00;
use utf8;
use Text::Unidecode;

sub import {
	hook(type => "getsetup", id => "i18nheadinganchors", call => \&getsetup);
	hook(type => "sanitize", id => "i18nheadinganchors", call => \&i18nheadinganchors);
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => undef,
			section => "widget",
		},
}

# turn given string into a "slug", retaining whitespace and original unicode script
# taken from https://stackoverflow.com/questions/4009281/how-can-i-generate-url-slugs-in-perl
sub slugify {
       my $input = shift;

       $input = unidecode($input);    # turn it into plain latin script
       $input =~ tr/\000-\177//cd;    # Strip non-ASCII characters (>127)
       $input =~ s/[^\w\s-]//g;       # Remove all characters that are not word characters (includes _), spaces, or hyphens
       $input =~ s/^\s+|\s+$//g;      # Trim whitespace from both ends
       $input = lc($input);           # Lowercase
       $input =~ s/[-\s]+/-/g;        # Replace all occurrences of spaces and hyphens with a single hyphen

       return $input;
}

sub i18nheadinganchors (@) {
	my %params=@_;
	my $content=$params{content};
	$content=~s{<h([0-9])(?:.*?)>([^>]*)</h([0-9])>}{'<h'.$1.' id="'.slugify($2).'">'.$2.'</h'.$3.'>'}gie;
	return $content;
}

1
